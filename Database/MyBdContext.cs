﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP1.Entities;

namespace TP1.Database
{
    class MyBdContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Editor> Editors { get; set; }
        public DbSet<BookShop> BookShops { get; set; }
        public DbSet<Author> Authors { get; set; }
        public MyBdContext()
        {
            
            GenerateBooks();
            GenerateCustomers();
            GenerateAuthors();
            GenerateEditors();
            GenerateBookShops();

        }

        public void GenerateBookShops()
        {
            for (int i = 0; i < 20; i++)
            {
                BookShops.Add(new BookShop() { Name = "El famoso BookShop" + i });
            }
            SaveChanges();
        }
        public void GenerateEditors()
        {
            for (int i = 0; i < 20; i++)
            {
                Editors.Add(new Editor() { Name = "Editruc" + i });
            }
            SaveChanges();
        }

        public void GenerateAuthors()
        {
            for (int i = 0; i < 20; i++)
            {
                Authors.Add(new Author() { Firstname = "Edouard" + i, Lastname = "Dubled" + i });
            }
            SaveChanges();
        }

        public void GenerateCustomers()
        {
            for (int i = 0; i < 20; i++)
            {
                Customers.Add(new Customer() { Firstname = "Jean" + i, Lastname = "Michel" + i });
            }
            SaveChanges();
        }

        public void GenerateBooks()
        {
            for (int i = 0; i < 20; i++)
            {
                Books.Add(new Book() { Name = "F" + i, NbPage = i });
            }
            SaveChanges();
        }
    }
}
