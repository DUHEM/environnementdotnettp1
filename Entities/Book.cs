﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1.Entities
{
    class Book
    {
        private string name;
        private long nbPage;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public long Id { get; set; }
        [Column("name")]
        [Required]
        public string Name { get => name; set => name = value; }
        [Column("nb_page")]
        [Required]
        public long NbPage { get => nbPage; set => nbPage = value; }
        
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
